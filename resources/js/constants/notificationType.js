export const IMAGE_PROCESSED = 'App\\Notifications\\ImageProcessedNotification';
export const IMAGE_PROCESSING_FAILED = 'App\\Notifications\\ImageProcessingFailedNotification';