<h2>Dear {{$user->name}},</h2>

<p>The applying the filter "{{$filter}}" was failed to the image:</p>

<a href="{{ $image['src'] }}"><img src="{{$image['src']}}" alt="Image"></a>

<p>Best regards,<br>
Binary Studio Academy</p>