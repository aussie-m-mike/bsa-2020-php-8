<?php

namespace App\Jobs;

use App\Actions\Image\ApplyFilterResponse;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\Contracts\ImageApiService;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $image;
    protected $filter;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Image $image, string $filter)
    {
        $this->user = Auth::user();
        $this->image = $image;
        $this->filter = $filter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ImageApiService $imageApiService)
    {
        $result = $imageApiService->applyFilter($this->image->getSrc(), $this->filter);

        $image = new Image(
            $this->image->getId(),
            $result,
		);

        $this->user->notify(new ImageProcessedNotification($image));
    }

    public function failed()
    {
        $this->user->notify(new ImageProcessingFailedNotification($this->image, $this->filter));
    }
}
