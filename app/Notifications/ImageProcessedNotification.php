<?php

namespace App\Notifications;

use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;

class ImageProcessedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    protected $image;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast($notifable)
    {
        return new BroadcastMessage([
            'status' => 'success',
            'image' => [
                'id' => $this->image->getId(),
                'src' => $this->image->getSrc()
            ]
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}