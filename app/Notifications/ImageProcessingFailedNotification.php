<?php

namespace App\Notifications;

use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    protected $image;
    protected $filter;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Image $image, string $filter)
    {
        $this->image = $image;
        $this->filter = $filter;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'broadcast'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->view('emails.image-processing-failed', [
                        'user' => $notifiable,
                        'image' => $this->image->toArray(),
                        'filter' => $this->filter
                    ]);
    }

    public function toBroadcast($notifable)
    {
        return new BroadcastMessage([
            'status' => 'failed',
            'message' => 'Applying filter failed',
            'image' => $this->image->toArray()
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
