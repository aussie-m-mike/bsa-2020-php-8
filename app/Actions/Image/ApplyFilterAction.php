<?php

declare(strict_types=1);

namespace App\Actions\Image;

use App\Jobs\ImageJob;
use App\Values\Image;

class ApplyFilterAction
{
	public function execute(Image $image, string $filter): void
	{
	    ImageJob::dispatch($image, $filter);
	}
}
